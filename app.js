require("dotenv").config();
require("./config/database").connect();
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const auth = require("./middleware/auth");
const axios = require("axios");
const cors = require("cors");
// importing user context
const User = require("./model/user");

const app = express();

app.use(express.json());
const corsOpts = {
  origin: "*",
  methods: ["GET", "POST"],
  allowedHeaders: ["Content-Type", "x-access-token"],
};

app.use(cors(corsOpts));
// Logic goes here

// Register
app.post("/register", async (req, res) => {
  // Our register logic starts here
  try {
    // Get user input
    const { first_name, last_name, email, password } = req.body;

    // Validate user input
    if (!(email && password && first_name && last_name)) {
      res.status(400).json({ error: "All input is required" });
    }

    // check if user already exist
    // Validate if user exist in our database
    const oldUser = await User.findOne({ email });

    if (oldUser) {
      return res.status(409).json({ error: "Email already registered." });
    }

    //Encrypt user password
    encryptedPassword = await bcrypt.hash(password, 10);

    // Create user in our database
    const user = await User.create({
      first_name,
      last_name,
      email: email.toLowerCase(), // sanitize: convert email to lowercase
      // password: encryptedPassword,
    });

    // Create token
    const token = jwt.sign(
      { user_id: user._id, email },
      process.env.TOKEN_KEY,
      {
        expiresIn: "2h",
      }
    );

    // save user token
    user.token = token;

    // return new user
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
  // Our register logic ends here
});

// Login
app.post("/login", async (req, res) => {
  // Our login logic starts here
  try {
    // Get user input
    const { email, password } = req.body;

    // Validate user input
    if (!(email && password)) {
      res.status(400).send("All input is required");
    }
    // Validate if user exist in our database
    const user = await User.findOne({ email });

    if (user && (await bcrypt.compare(password, user.password))) {
      // Create token
      const token = jwt.sign(
        { user_id: user._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );

      // save user token
      // user.token = token;

      // user
      res.status(200).json({
        token: token, 
        user: user
      });
    }
    res.status(400).json({ error: "Invalid credentials" });
  } catch (err) {
    console.log(err);
  }
  // Our register logic ends here
});

app.get("/countries/:country", auth, async (req, res) => {
  let country_data = await axios.get(
    "https://restcountries.com/v3.1/name/" + req.params.country
  );

  if (country_data.status == 200) {
    const data = {
      name: country_data.data[0].name.official,
      population: country_data.data[0].population,
      currencies: country_data.data[0].currencies,
    };

    const currency = Object.keys(country_data.data[0].currencies)[0];
    const exchange = await axios.get(
      "http://data.fixer.io/api/latest?access_key=" + process.env.FIXR_API_KEY
    );

    if (exchange.data.success == true) {
      const rate = exchange.data.rates[currency];
      data.exchange_rate = rate;
    }

    res.status(200).json(data);
  } else {
    res.status(404).json({ message: "No data found" });
  }
});

module.exports = app;
